//
//  SettingsViewController.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 4/9/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "SettingsViewController.h"

#define kTranspositionSliderOffsetFromCenter 12

@interface SettingsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *timingShiftLabel;
@property (weak, nonatomic) IBOutlet UISlider *timingShiftSlider;
@property (weak, nonatomic) IBOutlet UILabel *halfStepsLabel;
@property (weak, nonatomic) IBOutlet UISlider *transpositionSlider;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@property (nonatomic, assign) NSInteger timingShift;
@property (nonatomic, assign) NSInteger transposition;

@end

@implementation SettingsViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    if (!iPad) {
        self.slidingContainerViewWidthConstraint.constant = 340.0f;
        [self.view layoutIfNeeded];
//    }
    
    [self _setupDoneButton];
    
    _timingShift = 0;
    _transposition = kTranspositionSliderOffsetFromCenter;
}

#pragma mark - Setup

- (void)_setupDoneButton {
    _doneButton.layer.cornerRadius = _doneButton.bounds.size.height/2.0f;
    _doneButton.layer.borderColor = [_doneButton titleColorForState:UIControlStateNormal].CGColor;
    _doneButton.layer.borderWidth = 2.0f;
}

#pragma mark - Custom Setters & Getters

- (void)setTransposition:(NSInteger)transposition {
    if (_transposition != transposition && transposition >= 0 && transposition <= 24) {
        _transposition = transposition;
        
        if (transposition != kTranspositionSliderOffsetFromCenter) {
            _halfStepsLabel.text = [NSString stringWithFormat:@"%+.0f half step(s)", (float)transposition - kTranspositionSliderOffsetFromCenter];
        } else {
            _halfStepsLabel.text = [NSString stringWithFormat:@"no transposition"];
        }
        _transpositionSlider.value = (float)transposition;
        
        if ([self.delegate respondsToSelector:@selector(settingsViewController:didTransposition:)]) {
            [self.delegate settingsViewController:self didTransposition:(transposition - kTranspositionSliderOffsetFromCenter)];
        }
    }
}

- (void)setTimingShift:(NSInteger)timingShift {
    if (_timingShift != timingShift && _timingShift >= 0 && _timingShift <= 50) {
        _timingShift = timingShift;
        
        _timingShiftLabel.text = [NSString stringWithFormat:@"-%lu ms", timingShift];
        _timingShiftSlider.value  = (float)timingShift;
        
        if ([self.delegate respondsToSelector:@selector(settingsViewController:didChangeAccompTimingShift:)]) {
            [self.delegate settingsViewController:self didChangeAccompTimingShift:timingShift];
        }
    }
}

#pragma mark - Actions

- (IBAction)timingShiftSliderValueChanged:(UISlider *)sender {
    int discreteValue = roundl([sender value]);
    [sender setValue:(float)discreteValue];
    
    [self setTimingShift:discreteValue];
}

- (IBAction)transpositionSliderValueChanged:(UISlider *)sender {
    int discreteValue = roundl([sender value]);
    [sender setValue:(float)discreteValue];
    
    [self setTransposition:discreteValue];
}

- (IBAction)doneButtonAction:(id)sender {
    if ([self.slidingPopUpDelegate respondsToSelector:@selector(slidingPopUpControllerDidAskToHide:)]) {
        [self.slidingPopUpDelegate slidingPopUpControllerDidAskToHide:self];
    }
}

- (IBAction)resetButtonAction:(id)sender {
    [self.delegate settingsViewControllerDidAskToReset:self];
}

- (IBAction)decreaseTimingShiftButtonAction:(id)sender {
    self.timingShift = MAX(0, self.timingShift - 1);
}

- (IBAction)increaseTimingShiftButtonAction:(id)sender {
    self.timingShift = MIN(50, self.timingShift + 1);
}

- (IBAction)decreaseTranspotionButtonAction:(id)sender {
    self.transposition = MAX(0, self.transposition - 1);
}

- (IBAction)increaseTranspositionButtonAction:(id)sender {
    self.transposition = MIN(24, self.transposition + 1);
}

@end

@implementation SettingsCircleView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.cornerRadius = self.bounds.size.height/2.0f;
}

@end
