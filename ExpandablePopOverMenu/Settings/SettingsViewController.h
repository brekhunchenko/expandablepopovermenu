//
//  SettingsViewController.h
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 4/9/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "SlidingPopUpViewController.h"

@protocol SettingsViewControllerDelegate;

@interface SettingsViewController : SlidingPopUpViewController

@property (nonatomic, weak) id<SettingsViewControllerDelegate> delegate;

@end

@protocol SettingsViewControllerDelegate < NSObject >

- (void)settingsViewController:(SettingsViewController *)vc didChangeAccompTimingShift:(NSInteger)value;
- (void)settingsViewController:(SettingsViewController *)vc didTransposition:(NSInteger)value;
- (void)settingsViewControllerDidAskToReset:(SettingsViewController *)vc;

@end

@interface SettingsCircleView : UIView

@end
