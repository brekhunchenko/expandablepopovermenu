//
//  ViewController.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 10/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ViewController.h"
#import "ExpandableTableViewController.h"
#import "TuningViewController.h"
#import "MMPracticeLiveSegmentedControl.h"
#import "SettingsViewController.h"

#import <AVFoundation/AVFoundation.h>

typedef NS_ENUM(NSInteger, ExpandableTableViewControllerState) {
    ExpandableTableViewControllerStateHidden,
    ExpandableTableViewControllerStateShown
};

@interface ViewController () < UIAdaptivePresentationControllerDelegate, ExpandableTableViewControllerDelegate, TuningViewControllerDelegate, SlidingPopUpViewControllerDelegate, SettingsViewControllerDelegate >

@property (nonatomic, strong) ExpandableTableViewController* expandableViewController;
@property (nonatomic, strong) TuningViewController* tuningViewController;
@property (nonatomic, strong) SettingsViewController* settingsViewController;

@property (nonatomic, strong) AVAudioPlayer* audioPlayer;
@property (weak, nonatomic) IBOutlet MMPracticeLiveSegmentedControl *practiceLiveSegmentedControl;
    
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
    
@property (nonatomic, assign) BOOL isPlaying;
@property (nonatomic, assign) BOOL isRecording;

@end

@implementation ViewController

#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _setupExpandableViewController];
    [self _setupTuningViewController];
    [self _setupSettingsViewController];
//    [self _setupAudioPlayer];
}

#pragma mark - Setup

- (void)_setupSettingsViewController {
    _settingsViewController = [SettingsViewController new];
    _settingsViewController.slidingPosition = SlidingPopUpViewControllerPositionRight;
    _settingsViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    _settingsViewController.delegate = self;
    _settingsViewController.slidingPopUpDelegate = self;
    _settingsViewController.offsetFromVerticalCenter = iPad ? -200.0 : -8.0;
}

- (void)_setupTuningViewController {
    _tuningViewController = [TuningViewController new];
    _tuningViewController.slidingPosition = SlidingPopUpViewControllerPositionLeft;
    _tuningViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    _tuningViewController.delegate = self;
    _tuningViewController.slidingPopUpDelegate = self;
    _tuningViewController.offsetFromVerticalCenter = iPad ? 300.0f : 8.0;
}

- (void)_setupExpandableViewController {
    _expandableViewController = [ExpandableTableViewController instance];
    _expandableViewController.slidingPosition = SlidingPopUpViewControllerPositionRight;
    _expandableViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    _expandableViewController.delegate = self;
    _expandableViewController.slidingPopUpDelegate = self;
    _expandableViewController.offsetFromVerticalCenter = iPad ? 160.0: 8.0;
}

- (void)_setupAudioPlayer {
    NSString* soundFileName = @"A440Hzoboe_for_tuning.m4a";
    NSError *error;
    NSURL* audioURL = [[NSBundle mainBundle] URLForResource:soundFileName withExtension:@""];
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioURL error:&error];
}

#pragma mark - Actions

- (IBAction)reviewButtonAction:(id)sender {
    [self presentViewController:_expandableViewController animated:NO completion:nil];
}

- (IBAction)tuningButtonAction:(id)sender {
    [self presentViewController:_tuningViewController animated:NO completion:nil];
}

- (IBAction)practiceLiveSegmentedControlValueChanged:(id)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)playButtonAction:(id)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    _isPlaying = !_isPlaying;
    
    _playButton.selected = _isPlaying;
}
    
- (IBAction)recordButtonAction:(id)sender {
    _isRecording = !_isRecording;
    
    _recordButton.selected = _isRecording;
}
    
- (IBAction)libraryButtonAction:(id)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)settingsButtonAction:(id)sender {
    [self presentViewController:_settingsViewController animated:NO completion:nil];
}

#pragma mark - TuningViewControllerDelegate

- (void)tuningViewController:(TuningViewController *)vc didAskToPlayFrequency:(TuningFrequency)frequency {
    [_audioPlayer play];
}

- (void)tuningViewControllerDidAskToStopAudio:(TuningViewController *)vc {
    [_audioPlayer stop];
}

#pragma mark - ExpandableTableViewControllerDelegate

- (void)expandableTableViewControllerDidAskDelete:(ExpandableTableViewController *)vc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

#pragma mark - SettingsViewControllerDelegate

- (void)settingsViewControllerDidAskToReset:(SettingsViewController *)vc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)settingsViewController:(SettingsViewController *)vc didTransposition:(NSInteger)value {
    NSLog(@"User did select transposition: %d", value);
}

- (void)settingsViewController:(SettingsViewController *)vc didChangeAccompTimingShift:(NSInteger)value {
    NSLog(@"User did select timing shift: %d", value);
}

#pragma mark - SlidingPopUpViewControllerDelegate

- (void)slidingPopUpControllerDidAskToHide:(SlidingPopUpViewController *)vc {
    [vc dismissViewControllerAnimated:YES completion:nil];
}

@end
