//
//  SlidingPopUpViewController.h
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 3/1/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SlidingPopUpViewControllerPosition) {
    SlidingPopUpViewControllerPositionLeft,
    SlidingPopUpViewControllerPositionRight
};

@protocol SlidingPopUpViewControllerDelegate;

@interface SlidingPopUpViewController : UIViewController

@property (nonatomic, assign) SlidingPopUpViewControllerPosition slidingPosition;

@property (nonatomic, weak) id<SlidingPopUpViewControllerDelegate> slidingPopUpDelegate;

@property (weak, nonatomic) IBOutlet UIView *slidingContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *slidingContainerViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *slidingContainerViewHeightConstraint;

@property (nonatomic, assign) CGFloat offsetFromVerticalCenter; //default is 100.

@end

@protocol SlidingPopUpViewControllerDelegate <NSObject>

- (void)slidingPopUpControllerDidAskToHide:(SlidingPopUpViewController *)vc;

@end
