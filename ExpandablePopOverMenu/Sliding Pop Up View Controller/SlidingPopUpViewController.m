//
//  SlidingPopUpViewController.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 3/1/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "SlidingPopUpViewController.h"

typedef NS_ENUM(NSInteger, SlidingPopUpViewControllerState) {
    SlidingPopUpViewControllerHidden,
    SlidingPopUpViewControllerShown
};

@interface SlidingPopUpViewController () < UIGestureRecognizerDelegate >

@property (nonatomic, assign) SlidingPopUpViewControllerState slidingContainerViewState;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *slidingContainerViewSideConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *slidingContainerViewYCenterConstraint;

@property (nonatomic, strong) UIPanGestureRecognizer* panGestureRecognizer;
@property (nonatomic, assign) CGPoint previousLocation;

@end

@implementation SlidingPopUpViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
        
    [self _setupGestureRecognizers];
    [self _setExpandableTableViewControllerState:SlidingPopUpViewControllerHidden slidingPosition:_slidingPosition animation:NO completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self _setExpandableTableViewControllerState:SlidingPopUpViewControllerHidden slidingPosition:_slidingPosition animation:NO completion:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self _setExpandableTableViewControllerState:SlidingPopUpViewControllerShown slidingPosition:_slidingPosition animation:YES completion:nil];
}

#pragma mark - Setup

- (void)_setupGestureRecognizers {
    UIPanGestureRecognizer* panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(_panGestureRecognizerCatched:)];
    panGestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:panGestureRecognizer];
}

#pragma mark - Gestures

- (void)_panGestureRecognizerCatched:(UIPanGestureRecognizer *)panGestureRecognizer {
    static BOOL shouldMoveExpandablePopup = NO;
    CGPoint locationInView = [panGestureRecognizer locationInView:self.view];
    BOOL isExpandablePopupNavBarLocation = [panGestureRecognizer locationInView:self.slidingContainerView].y < 84.0f && [panGestureRecognizer locationInView:self.slidingContainerView].y > 0;
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan) {
        if (isExpandablePopupNavBarLocation) {
            shouldMoveExpandablePopup = YES;
            _previousLocation = locationInView;
        } else {
            shouldMoveExpandablePopup = NO;
        }
    }
    
    if (shouldMoveExpandablePopup) {
        if (CGPointEqualToPoint(_previousLocation, CGPointZero) == NO) {
            CGPoint movement = CGPointMake(locationInView.x - _previousLocation.x, locationInView.y - _previousLocation.y);
            self.slidingContainerView.center = CGPointMake(self.slidingContainerView.center.x + movement.x, self.slidingContainerView.center.y + movement.y);
        }
        _previousLocation = locationInView;
    }
}

#pragma mark - Sliding

- (void)_setExpandableTableViewControllerState:(SlidingPopUpViewControllerState)state
                               slidingPosition:(SlidingPopUpViewControllerPosition)slidingPosition
                                     animation:(BOOL)animated completion:(void (^)(void))completion {
    _slidingContainerViewState = state;
    
    typeof(self) weakSelf = self;
    void(^slidingAnimation)(void) = ^() {
        CGFloat offsetFromSide = 32;
        if (slidingPosition == SlidingPopUpViewControllerPositionRight) {
            weakSelf.slidingContainerViewYCenterConstraint.constant = _offsetFromVerticalCenter;
            weakSelf.slidingContainerViewSideConstraint.constant = (state == SlidingPopUpViewControllerHidden ? weakSelf.slidingContainerView.frame.size.width : -offsetFromSide);
        } else {
            weakSelf.slidingContainerViewYCenterConstraint.constant = _offsetFromVerticalCenter;
            if (state == SlidingPopUpViewControllerShown) {
                weakSelf.slidingContainerViewSideConstraint.constant = offsetFromSide;
            } else {
                weakSelf.slidingContainerViewSideConstraint.constant = -weakSelf.slidingContainerView.frame.size.width - offsetFromSide;
            }
        }
        [self.view layoutIfNeeded];
    };
    
    if (animated) {
        [UIView animateWithDuration:0.5 animations:^{
            slidingAnimation();
        } completion:^(BOOL finished) {
            if (completion) completion();
        }];
    } else {
        slidingAnimation();
        if (completion) completion();
    }
}

#pragma mark - Actions

- (IBAction)minimizeButtonAction:(id)sender {
    if ([self.slidingPopUpDelegate respondsToSelector:@selector(slidingPopUpControllerDidAskToHide:)]) {
        [self.slidingPopUpDelegate slidingPopUpControllerDidAskToHide:self];
    }
}

#pragma mark - Overridden

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    [self _setExpandableTableViewControllerState:SlidingPopUpViewControllerHidden slidingPosition:_slidingPosition animation:flag completion:^{
        [super dismissViewControllerAnimated:NO completion:completion];
    }];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    BOOL isExpandablePopupNavBarLocation = [gestureRecognizer locationInView:self.slidingContainerView].y < 84.0f && [gestureRecognizer locationInView:self.slidingContainerView].y > 0;
    return isExpandablePopupNavBarLocation;
}

@end
