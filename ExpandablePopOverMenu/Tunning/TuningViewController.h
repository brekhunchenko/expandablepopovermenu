//
//  TuningViewController.h
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 2/24/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingPopUpViewController.h"

typedef NS_ENUM(NSInteger, TuningFrequency) {
    TuningFrequency440Hz = 440,
    TuningFrequency441Hz,
    TuningFrequency442Hz,
    TuningFrequency443Hz,
    TuningFrequency444Hz
};

@protocol TuningViewControllerDelegate;

@interface TuningViewController : SlidingPopUpViewController

@property (nonatomic, weak) id<TuningViewControllerDelegate> delegate;

@end

@protocol TuningViewControllerDelegate < NSObject >

- (void)tuningViewController:(TuningViewController *_Nonnull)vc didAskToPlayFrequency:(TuningFrequency)frequency;
- (void)tuningViewControllerDidAskToStopAudio:(TuningViewController *_Nonnull)vc;

@end
