//
//  TuningViewController.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 2/24/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "TuningViewController.h"

@interface TuningViewController ()

@property (nonatomic, assign) TuningFrequency selectedFrequency;

@property (weak, nonatomic) IBOutlet UILabel *frequencyLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (nonatomic, assign) BOOL playing;

@end

@implementation TuningViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setSelectedFrequency:TuningFrequency440Hz];
    
    [self _setupPlayButton];
}

#pragma mark - Setup

- (void)_setupPlayButton {
    _playButton.layer.cornerRadius = 5.0;
    _playButton.layer.borderColor = [UIColor colorWithRed:102.0f/255.0f green:45.0f/255.0f blue:145.0f/255.0f alpha:1.0].CGColor;
    _playButton.layer.borderWidth = 2.0f;
}

#pragma mark - Custom Setters & Getters

- (void)setSelectedFrequency:(TuningFrequency)selectedFrequency {
    _selectedFrequency = selectedFrequency;
    
    _frequencyLabel.text = [self _titleForFrequency:_selectedFrequency];
}

#pragma mark - Actions

- (IBAction)decreaseFrequencyButtonAction:(id)sender {
    self.selectedFrequency = MAX(TuningFrequency440Hz, _selectedFrequency - 1);
}

- (IBAction)increaseFrequencyButtonAction:(id)sender {
    self.selectedFrequency = MIN(TuningFrequency444Hz, _selectedFrequency + 1);
}

- (IBAction)playButtonAction:(UIButton *)sender {
    if (self.playing) {
        self.playing = NO;
        [_playButton setTitle:@"Play" forState:UIControlStateNormal];
        
        if ([self.delegate respondsToSelector:@selector(tuningViewControllerDidAskToStopAudio:)]) {
            [self.delegate tuningViewControllerDidAskToStopAudio:self];
        }
    } else {
        self.playing = YES;
        [_playButton setTitle:@"Stop" forState:UIControlStateNormal];
        
        if ([self.delegate respondsToSelector:@selector(tuningViewController:didAskToPlayFrequency:)]) {
            [self.delegate tuningViewController:self didAskToPlayFrequency:_selectedFrequency];
        }
    }
}

#pragma mark - Utils

- (NSString *)_titleForFrequency:(TuningFrequency)frequency {
    switch (frequency) {
        case TuningFrequency440Hz: return @"440 Hz";
        case TuningFrequency441Hz: return @"441 Hz";
        case TuningFrequency442Hz: return @"442 Hz";
        case TuningFrequency443Hz: return @"443 Hz";
        case TuningFrequency444Hz: return @"444 Hz";
            
        default: break;
    }
}

@end
