//
//  ExpandableTableViewController.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 10/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ExpandableTableViewController.h"
#import "SLExpandableTableView.h"

#import "ExpandableTableViewCell.h"
#import "MusicPieceInfoTableViewCell.h"
#import "ReviewTableViewUploadCell.h"

@interface ExpandableTableViewController () < SLExpandableTableViewDelegate, SLExpandableTableViewDatasource, MusicPieceInfoTableViewCellDelegate >

@property (weak, nonatomic) IBOutlet SLExpandableTableView *tableView;

@end

@implementation ExpandableTableViewController

#pragma mark - Initializers

+ (instancetype)instance {
    ExpandableTableViewController* vc = [[UIStoryboard storyboardWithName:@"ExpandablePopOverMenu" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ExpandableTableViewController class])];
    return vc;
}

#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!iPad) {
        self.slidingContainerViewWidthConstraint.constant = self.view.frame.size.width - 16.0*2.0;
        self.slidingContainerViewHeightConstraint.constant = 500.0f;
        [self.view layoutIfNeeded];
    }
    
    [self _setupTableView];
}

#pragma mark - Setup

- (void)_setupTableView {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

#pragma mark - SLExpandableTableViewDatasource

- (BOOL)tableView:(SLExpandableTableView *)tableView canExpandSection:(NSInteger)section {
    return YES;
}

- (BOOL)tableView:(SLExpandableTableView *)tableView needsToDownloadDataForExpandableSection:(NSInteger)section {
    return NO;
}

- (UITableViewCell<UIExpandingTableViewCell> *)tableView:(SLExpandableTableView *)tableView expandingCellForSection:(NSInteger)section {
//    ExpandableTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ExpandableTableViewCell class])];
//    __weak typeof(self) weakSelf = self;
//    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor] callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
//        if ([weakSelf.delegate respondsToSelector:@selector(expandableTableViewControllerDidAskDelete:)]) {
//            [weakSelf.delegate expandableTableViewControllerDidAskDelete:weakSelf];
//        }
//        return YES;
//    }]];
//    cell.rightSwipeSettings.transition = MGSwipeTransitionStatic;
//    return cell;
    ReviewTableViewUploadCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ReviewTableViewUploadCell class])];
    __weak typeof(self) weakSelf = self;
    cell.uploadState = section%3;
    cell.isLivePerfomance = section%2 == 0;
    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor] callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
        if ([weakSelf.delegate respondsToSelector:@selector(expandableTableViewControllerDidAskDelete:)]) {
            [weakSelf.delegate expandableTableViewControllerDidAskDelete:weakSelf];
        }
        return YES;
    }]];
    cell.rightSwipeSettings.transition = MGSwipeTransitionStatic;
    return cell;
}

#pragma mark - SLExpandableTableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        static ReviewTableViewUploadCell *cell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ReviewTableViewUploadCell class])];
        });
        return [cell cellHeight];
    } else {
        static MusicPieceInfoTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MusicPieceInfoTableViewCell class])];
        });
        return [cell cellHeight];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 10;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MusicPieceInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MusicPieceInfoTableViewCell class])];
    cell.delegate = self;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)tableView:(SLExpandableTableView *)tableView didExpandSection:(NSUInteger)section animated:(BOOL)animated {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - MusicPieceInfoTableViewCellDelegate

- (void)musicPieceInfoTableViewCellAskToPlay:(MusicPieceInfoTableViewCell *)cell {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end
