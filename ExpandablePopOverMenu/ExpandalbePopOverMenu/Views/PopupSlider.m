//
//  PopupSlider.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 1/31/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "PopupSlider.h"

@implementation PopupSlider

#pragma mark - Initializers

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setThumbImage:[UIImage imageNamed:@"popup_slider_thumb_image"] forState:UIControlStateNormal];
    [self setThumbImage:[UIImage imageNamed:@"popup_slider_thumb_image"] forState:UIControlStateSelected];
    [self setThumbImage:[UIImage imageNamed:@"popup_slider_thumb_image"] forState:UIControlStateHighlighted];
}

@end
