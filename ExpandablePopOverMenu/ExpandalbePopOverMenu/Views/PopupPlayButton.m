//
//  PopupPlayButton.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 2/2/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "PopupPlayButton.h"

#define DEGREES_TO_RADIANS(degrees)((M_PI * degrees)/180)

@interface PopupPlayButton()

@property (nonatomic, strong) CAShapeLayer* grayCircle;
@property (nonatomic, strong) CAShapeLayer* progressCircle;

@end

@implementation PopupPlayButton

#pragma mark - Initializers

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self _setupShapeLayers];    
}

#pragma mark - Setup

- (void)_setupShapeLayers {
    _grayCircle = [CAShapeLayer layer];
    _grayCircle.strokeColor = [UIColor colorWithRed:204.0f/255.0f green:188.0f/255.0f blue:218.0f/255.0f alpha:1.0f].CGColor;
    _grayCircle.fillColor = [UIColor clearColor].CGColor;
    _grayCircle.lineWidth = 2.0;
    UIBezierPath* path = [UIBezierPath bezierPathWithOvalInRect:self.bounds];
    _grayCircle.path = path.CGPath;
    [self.layer addSublayer:_grayCircle];
    
    _progressCircle = [CAShapeLayer layer];
    _progressCircle.strokeColor = [UIColor colorWithRed:102.f/255.0f green:45.0f/255.0f blue:144.0f/255.0f alpha:1.0f].CGColor;
    _progressCircle.fillColor = [UIColor clearColor].CGColor;
    _progressCircle.lineWidth = 2.0;
    _progressCircle.path = [self _pathForProgress:1.0].CGPath;
    [self.layer addSublayer:_progressCircle];
}

#pragma mark - Custom Setters & Getters

- (void)setMode:(PopupPlayButtonMode)mode {
    _mode = mode;
    
    if (mode == PopupPlayButtonModePlay) {
        [self setImage:[UIImage imageNamed:@"popup_play_icon_empty"] forState:UIControlStateNormal];
    } else {
        [self setImage:[UIImage imageNamed:@"popup_pause_icon"] forState:UIControlStateNormal];
    }
}

- (void)setProgress:(CGFloat)progress {
    _progress = MAX(0.0, MIN(1.0, progress));
    
    _progressCircle.path = [self _pathForProgress:_progress].CGPath;
}

#pragma mark - Private Methods

- (UIBezierPath *)_pathForProgress:(CGFloat)progress {
    UIBezierPath* path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.bounds.size.width/2.0f, self.bounds.size.height/2.0)
                                                        radius:self.bounds.size.width/2.0
                                                    startAngle:-M_PI_2
                                                      endAngle:(DEGREES_TO_RADIANS(360.0*progress) - M_PI_2)
                                                     clockwise:YES];
    return path;
}

@end
