//
//  PopupSlider.h
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 1/31/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupSlider : UISlider

@end
