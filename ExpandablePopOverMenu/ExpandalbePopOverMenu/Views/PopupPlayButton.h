//
//  PopupPlayButton.h
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 2/2/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PopupPlayButtonMode){
    PopupPlayButtonModePlay,
    PopupPlayButtonModePause
};

@interface PopupPlayButton : UIButton

@property (nonatomic, assign) PopupPlayButtonMode mode;
@property (nonatomic, assign) CGFloat progress;

@end
