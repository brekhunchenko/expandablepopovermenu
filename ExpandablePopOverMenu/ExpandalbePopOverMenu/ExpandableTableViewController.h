//
//  ExpandableTableViewController.h
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 10/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingPopUpViewController.h"

@protocol ExpandableTableViewControllerDelegate;

@interface ExpandableTableViewController : SlidingPopUpViewController

+ (instancetype)instance;

@property (nonatomic, weak) id<ExpandableTableViewControllerDelegate> delegate;

@end

@protocol ExpandableTableViewControllerDelegate < NSObject >

- (void)expandableTableViewControllerDidAskDelete:(ExpandableTableViewController *)vc;

@end
