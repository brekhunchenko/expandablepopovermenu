//
//  ExpandableTableViewCell.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 10/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ExpandableTableViewCell.h"
#import "PopupPlayButton.h"

@interface ExpandableTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *mmLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet PopupPlayButton *playButton;

@end

@implementation ExpandableTableViewCell

#pragma mark - Initialization

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.attributedText = [self attributedStringForTitle:@"Take 3" date:[NSDate date]];
    
    [_playButton setMode:PopupPlayButtonModePlay];
    _playButton.progress = [self randomFloatBetween:0.0 and:1.0];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [_playButton setMode:PopupPlayButtonModePlay];
    _playButton.progress = [self randomFloatBetween:0.0 and:1.0];
}

#pragma mark - Public Methods

- (CGFloat)cellHeight {
    return 111.0f;
}

#pragma mark - Private Methods

- (NSAttributedString *)attributedStringForTitle:(NSString *)title date:(NSDate *)date {
    NSMutableAttributedString* mutableAttributedString = [NSMutableAttributedString new];
    UIFont* titleFont = [UIFont systemFontOfSize:27.0];
    [mutableAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Take 3   " attributes:@{
                                                                                                                NSFontAttributeName : titleFont,
                                                                                                                NSForegroundColorAttributeName : [UIColor colorWithRed:49.0f/255.0f green:49.0f/255.0f blue:49.0f/255.0f alpha:1.0]
                                                                                                                }]];
    NSString* timeDateString = [[self _dateFormatter] stringFromDate:date];
    UIFont* timeDateFont = [UIFont systemFontOfSize:17.0];
    [mutableAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:timeDateString attributes:@{
                                                                                                                      NSFontAttributeName : timeDateFont,
                                                                                                                      NSForegroundColorAttributeName : [UIColor colorWithRed:137.0f/255.0f green:137.0f/255.0f blue:137.0f/255.0f alpha:1.0]
                                                                                                                      }]];
    return mutableAttributedString;
}

- (NSDateFormatter *)_dateFormatter {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"h:mm a, d MMM, yy";
    return dateFormatter;
}

#pragma mark - UIExpandingTableViewCell

- (void)setLoading:(BOOL)loading {
    if (loading != _loading) {
        _loading = loading;
    }
}

- (void)setExpansionStyle:(UIExpansionStyle)expansionStyle animated:(BOOL)animated {
    if (expansionStyle != _expansionStyle) {
        _expansionStyle = expansionStyle;
    }
}

#pragma mark - Actions

- (IBAction)playButtonAction:(PopupPlayButton *)button {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    button.mode = (button.mode == PopupPlayButtonModePlay ? PopupPlayButtonModePause : PopupPlayButtonModePlay);
}

#pragma mark - TEMP

- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber {
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

@end
