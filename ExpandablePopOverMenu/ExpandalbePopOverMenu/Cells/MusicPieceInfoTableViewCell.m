//
//  MusicPieceInfoTableViewCell.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 10/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MusicPieceInfoTableViewCell.h"

@interface MusicPieceInfoTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *textViewContainerView;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@end

@implementation MusicPieceInfoTableViewCell

#pragma mark - Initializers

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _textViewContainerView.layer.borderWidth = 1.0f;
    _textViewContainerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _textViewContainerView.layer.cornerRadius = 8.0f;
    
    _commentButton.layer.cornerRadius = _commentButton.bounds.size.height/2.0f;
    _commentButton.layer.borderColor = [_commentButton titleColorForState:UIControlStateNormal].CGColor;
    _commentButton.layer.borderWidth = 2.0f;
}

#pragma mark - Public Methods

- (CGFloat)cellHeight {
    return 310.0f;
}

#pragma mark - Actions

- (IBAction)playButtonAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(musicPieceInfoTableViewCellAskToPlay:)]) {
        [self.delegate musicPieceInfoTableViewCellAskToPlay:self];
    }
}

@end
