//
//  ReviewTableViewUploadCell.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 8/21/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ReviewTableViewUploadCell.h"
#import "PopupPlayButton.h"
#import "PopupUploadButton.h"
#import "MMPracticeLiveSegmentedControl.h"

@interface ReviewTableViewUploadCell()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *mmLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet PopupPlayButton *playButton;
@property (weak, nonatomic) IBOutlet PopupUploadButton *uploadButton;
@property (weak, nonatomic) IBOutlet MMPracticeLiveSegmentedControl *perfomanceTypeSegmentedControl;

@end

@implementation ReviewTableViewUploadCell

#pragma mark - Initialization

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.attributedText = [self attributedStringForTitle:@"Take 3" date:[NSDate date]];
    
    [_playButton setMode:PopupPlayButtonModePlay];
    _playButton.progress = [self randomFloatBetween:0.0 and:1.0];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.uploadState = ReviewTableViewUploadCellStateToUpload;
    
    [_playButton setMode:PopupPlayButtonModePlay];
    _playButton.progress = [self randomFloatBetween:0.0 and:1.0];
}

#pragma mark - Public Methods

- (CGFloat)cellHeight {
    return 160.0f;
}

#pragma mark - Private Methods

- (NSAttributedString *)attributedStringForTitle:(NSString *)title date:(NSDate *)date {
    NSMutableAttributedString* mutableAttributedString = [NSMutableAttributedString new];
    UIFont* titleFont = [UIFont systemFontOfSize:27.0];
    [mutableAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Take 3   " attributes:@{
                                                                                                                         NSFontAttributeName : titleFont,
                                                                                                                         NSForegroundColorAttributeName : [UIColor colorWithRed:49.0f/255.0f green:49.0f/255.0f blue:49.0f/255.0f alpha:1.0]
                                                                                                                         }]];
    NSString* timeDateString = [[self _dateFormatter] stringFromDate:date];
    UIFont* timeDateFont = [UIFont systemFontOfSize:17.0];
    [mutableAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:timeDateString attributes:@{
                                                                                                                           NSFontAttributeName : timeDateFont,
                                                                                                                           NSForegroundColorAttributeName : [UIColor colorWithRed:137.0f/255.0f green:137.0f/255.0f blue:137.0f/255.0f alpha:1.0]
                                                                                                                           }]];
    return mutableAttributedString;
}

- (NSDateFormatter *)_dateFormatter {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"h:mm a, d MMM, yy";
    return dateFormatter;
}

#pragma mark - UIExpandingTableViewCell

- (void)setUploadState:(ReviewTableViewUploadCellState)uploadState {
    _uploadState = uploadState;
    
    switch (_uploadState) {
        case ReviewTableViewUploadCellStateToUpload:
            self.uploadButton.alpha = 1.0;
            self.uploadButton.userInteractionEnabled = YES;
            
            break;
        
        case ReviewTableViewUploadCellStateUploaded:
            self.uploadButton.alpha = 0.0;
            self.uploadButton.userInteractionEnabled = NO;
            
            break;
            
        case ReviewTableViewUploadCellStateUploading:
            self.uploadButton.alpha = 0.5;
            self.uploadButton.userInteractionEnabled = NO;
            
            break;
    }
}

- (void)setIsLivePerfomance:(BOOL)isLivePerfomance {
    _isLivePerfomance = isLivePerfomance;
    
    self.perfomanceTypeSegmentedControl.selectedSegmentIndex = _isLivePerfomance ? 1 : 0;
}

- (void)setLoading:(BOOL)loading {
    if (loading != _loading) {
        _loading = loading;
    }
}

- (void)setExpansionStyle:(UIExpansionStyle)expansionStyle animated:(BOOL)animated {
    if (expansionStyle != _expansionStyle) {
        _expansionStyle = expansionStyle;
    }
}

#pragma mark - Actions

- (IBAction)perfomanceTypeSegmentedControlValueChanged:(MMPracticeLiveSegmentedControl *)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)uploadButtonAction:(id)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)playButtonAction:(PopupPlayButton *)button {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    button.mode = (button.mode == PopupPlayButtonModePlay ? PopupPlayButtonModePause : PopupPlayButtonModePlay);
}

#pragma mark - TEMP

- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber {
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

@end
