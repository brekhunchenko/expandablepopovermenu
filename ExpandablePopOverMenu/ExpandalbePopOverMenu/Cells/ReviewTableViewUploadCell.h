//
//  ReviewTableViewUploadCell.h
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 8/21/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLExpandableTableView.h"
#import "MGSwipeTableCell.h"

typedef NS_ENUM(NSInteger, ReviewTableViewUploadCellState) {
    ReviewTableViewUploadCellStateToUpload,
    ReviewTableViewUploadCellStateUploaded,
    ReviewTableViewUploadCellStateUploading
};

@interface ReviewTableViewUploadCell : MGSwipeTableCell < UIExpandingTableViewCell >

@property (nonatomic, assign, getter = isLoading) BOOL loading;
@property (nonatomic, assign) ReviewTableViewUploadCellState uploadState;
@property (nonatomic, assign) BOOL isLivePerfomance;
@property (nonatomic, readonly) UIExpansionStyle expansionStyle;
- (void)setExpansionStyle:(UIExpansionStyle)expansionStyle animated:(BOOL)animated;
- (CGFloat)cellHeight;

@end
