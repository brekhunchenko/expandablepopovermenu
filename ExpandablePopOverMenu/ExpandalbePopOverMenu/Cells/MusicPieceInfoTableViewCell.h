//
//  MusicPieceInfoTableViewCell.h
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 10/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MusicPieceInfoTableViewCellDelegate;

@interface MusicPieceInfoTableViewCell : UITableViewCell

@property (nonatomic, weak) id<MusicPieceInfoTableViewCellDelegate> delegate;

- (CGFloat)cellHeight;

@end

@protocol MusicPieceInfoTableViewCellDelegate <NSObject>

@optional
- (void)musicPieceInfoTableViewCellAskToPlay:(MusicPieceInfoTableViewCell *)cell;

@end

