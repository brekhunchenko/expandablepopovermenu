//
//  ExpandableTableViewCell.h
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 10/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLExpandableTableView.h"
#import "MGSwipeTableCell.h"

@interface ExpandableTableViewCell : MGSwipeTableCell < UIExpandingTableViewCell >

@property (nonatomic, assign, getter = isLoading) BOOL loading;

@property (nonatomic, readonly) UIExpansionStyle expansionStyle;
- (void)setExpansionStyle:(UIExpansionStyle)expansionStyle animated:(BOOL)animated;
- (CGFloat)cellHeight;

@end
