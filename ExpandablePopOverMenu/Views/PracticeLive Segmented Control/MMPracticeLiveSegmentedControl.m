//
//  PracticeLiveSegmentedControl.m
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 4/9/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMPracticeLiveSegmentedControl.h"

@implementation MMPracticeLiveSegmentedControl

#pragma mark - Initializers

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.cornerRadius = self.bounds.size.height/2.0;
    self.layer.borderColor = self.tintColor.CGColor;
    self.layer.borderWidth = 2.0;
    self.layer.masksToBounds = YES;
    
    [self setImage:[UIImage imageFromImage:[UIImage imageNamed:@"practice_icon_unselected"] string:@"Practice" color:[UIColor blackColor]] forSegmentAtIndex:0];
    [self setImage:[UIImage imageFromImage:[UIImage imageNamed:@"live_icon_selected"] string:@"Live" color:[UIColor blackColor]] forSegmentAtIndex:1];
}

@end

@implementation UIImage (UISegmentIconAndText)

+ (UIImage *)imageFromImage:(UIImage*)image string:(NSString*)string color:(UIColor*)color {
    UIFont *font = [UIFont systemFontOfSize:12.0];
    CGSize expectedTextSize = [string sizeWithAttributes:@{NSFontAttributeName: font}];
    int width = expectedTextSize.width + image.size.width + 5;
    int height = MAX(expectedTextSize.height, image.size.width);
    CGSize size = CGSizeMake((float)width, (float)height);
    UIGraphicsBeginImageContextWithOptions(size, NO, 2);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    int fontTopPosition = (height - expectedTextSize.height) / 2;
    CGPoint textPoint = CGPointMake(image.size.width + 5, fontTopPosition);
    
    [string drawAtPoint:textPoint withAttributes:@{NSFontAttributeName: font}];
    // Images upside down so flip them
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, size.height);
    CGContextConcatCTM(context, flipVertical);
    CGContextDrawImage(context, (CGRect){ {0, (height - image.size.height) / 2}, {image.size.width, image.size.height} }, [image CGImage]);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
