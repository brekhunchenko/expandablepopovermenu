//
//  PracticeLiveSegmentedControl.h
//  ExpandablePopOverMenu
//
//  Created by Yaroslav Brekhunchenko on 4/9/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMPracticeLiveSegmentedControl : UISegmentedControl

@end

@interface UIImage (UISegmentIconAndText)

+ (UIImage *)imageFromImage:(UIImage*)image string:(NSString*)string color:(UIColor*)color;

@end
